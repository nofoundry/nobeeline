# NoBeeline

<img src="/uploads/f58d6646eac08fd09e24327d743e9950/Capture_d_écran_2020-06-26_à_16.22.38.png" width="1000" style='margin: 30px' />

*Made by [Nina Overkott](http://ninaoverkott.world/). 
If you make anything using NoBeeline I would love to see it! You can find me [@ninaoverkott](https://www.instagram.com/ninaoverkott/) or ninaoverkott (at) gmail (dot) com
This code is open source and totally free to use in any way. However, if you use this for a project, feel free to tip your friendly neighbourhood coder on paypal (same email)!*

## Installation

If you aren't familiar with coding, you can directly download this zip file, use it as a tool and play around with some options, just follow these next few steps.
If you want to use this effect in a coding project, you can easily export the useful snippet, go to 5.
If you would like to use it in InDesign, go to 6.

#### 1. Download the whole website

You can do this by clicking the "Download" icon button up top, and then the "zip" option, like so:

<img src="/uploads/bf5879295231fe921f6e2f539aa8f290/Capture_d_écran_2020-06-26_à_14.21.52.png" width="400" />

#### 2. Open the file 'index.html'

*You can open & modify all the files in the folder in a simple text editor! But I recommend a code editor like Atom — it's free and open source.*

This is the place for your content. You can leave it all as it is and just place your text between these tags: 

     <div> Copy & paste your own text here! </div>

This is already enough to get a look! Just save your modifications then drag and drop your 'index.html' file into any browser. Any time you reload the page, it will look different. 

#### 3. Experiment

**With the style:**

To experiment with style, open your **main.css** file like you did the index file. Here, some values can be changed for styling. I put some notes (in grey) about what can be experimented with! 

**With the math:**

There are a number of variables you can change to get different results.
As it is by default, NoBeeline tends to go exponential with height. That is, it calculates the height of each letter in relation to the one before it, up until a certain value, then resets a bit, and so on. This is what give it this 'dropping' effect. 

If you would like a more random effect, or if you want to go more or less extreme, open your **main.js** file like you did the index file. I put some notes (in grey) about what can be experimented with! In case you want to go back to start, I made a note of all the default values.

Remember to always save before you reload the page!

**Go offline!**

You might want to use this tool as a print project. Keep in mind that this is a two-hour project so the options are limited for now, but here's how I go about it: Print the webpage, as a PDF. In the pop-up that opens, you should be able to also adjust margins, zoom, etc. Now you can import your PDF into InDesign, for example!

#### 5. Go online!

I recommend using You might also want to use this effect on some part of your own coding project. If so, all you need is to download the nobeeline.js file and importing the full file into your project by copying this line into your code:

     <script src="nobeeline.js"></script>

Please note that you will absolutely need jquery for the effect to work! If you already linked it in your project, nobeeline.js should come after it. If not, just link it like so: 

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <script src="nobeeline.js"></script>

Then, you can use the nobeeline function in your own javascript to randomize the rotation and line-height of html elements, like such:

     nobeeline(element);

For example: 
     
     nobeeline("p");
     nobeeline(".my-paragraphs");
     nobeeline("#title");
     nobeeline(".paragraph a");

<details><summary><b>It's bugging! What could be wrong?</b></summary>
 - Can you make sure this line: `<script src="nobeeline.js"></script>` comes after the jquery import and before all of your other scripts? If not, it should be! Indeed, the nobeeline("div") function has to be defined before it can be used anywhere.
</details>





