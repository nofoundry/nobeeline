// For developpers: update the tag between the "" to use the effect on your own code
nobeeline("div");

function nobeeline(tag) {
  $(tag).each(function() {
    var txt = $(this).text();
    var height = Math.random() * 20;
    // To also play with the rotation of the letters, change this 0 to a 2 (or any other number)
    rotation = 2;
    var newTxt = txt.replace(/\w/g,function(c){
      // In the following line, decreasing the number will decrease the letters' maximum distance to the base line. That is, decreasing the intensity of the effect. Increasing will in turn make it more intense.
      if (height <= 80 ) {
        // In the following line, removing the 'height *' part will remove the exponentiality. You can also experiment with changing the numbers! Just in case, to reset, here's a note of the default value: (Math.random() * - 0.69 + 1.38)
        height = height * (Math.random() * - 0.69 + 1.38);
        // In the following line, removing the 'rotation *' part will remove the exponentiality. You can also experiment with changing the numbers! Just in case, to reset, here's a note of the default value: (Math.random() * - 0.7 + 1.4)
        rotation = rotation * (Math.random() * - 0.7 + 1.4);
      }
      else {
        // Here, you can also experiment with the number. To reset, default value: (Math.random() * - 2 + 4)
        height = (Math.random() * - 2 + 4);
        // Here, you can also experiment with the number. Removing the "* rotation will cancel the exponentiality". To reset, default value: - (Math.random() * 10) * rotation
        rotation = - (Math.random() * 10) * rotation;
      }
      var spanning = "<span style='display: inline-block; transform: translateY(" + height +"px) rotate(" + rotation + "deg)'>"
      return spanning + c + '</span>';
    })
    $(this).html(newTxt);
  })
};